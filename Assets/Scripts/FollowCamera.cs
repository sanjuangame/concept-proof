using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {

    public Transform target;
    private Vector3 camPosition;
    public float distance = 10f; 

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        if (target != null)
        {
            transform.position = new Vector3(target.transform.position.x, target.transform.position.y,target.transform.position.z - distance);
        }
	
	}
}
