using UnityEngine;
using System.Collections;

public class KillPlayerByThorn : MonoBehaviour {

    private GameObject deadString;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            Destroy(coll.gameObject);
            deadString = transform.parent.transform.GetComponentInParent<ParentReference>().reference;
            deadString.SetActive(true);
        }

    }
}
