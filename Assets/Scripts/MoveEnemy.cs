using UnityEngine;
using System.Collections;

public class MoveEnemy : MonoBehaviour {

    public float velocity;
    public float initialTime;
    private float timeleft;
    private int sense;
    private bool reset = true;
    private GameObject deadString;

	// Use this for initialization
	void Start () {
        sense = 1;
        timeleft = initialTime;
    }
	
	// Update is called once per frame
	void Update () {
            
        timeleft -= Time.deltaTime;
        if (timeleft < 0)
        {
            sense *= -1;           
            reset = true;
        }
        resetTimeControl();
        reset = false;
        transform.Translate(new Vector3 (sense,0,0) * Time.deltaTime * velocity);
	}

    void resetTimeControl()
    {
        if (reset)
        {
            timeleft = initialTime;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            Destroy(coll.gameObject);
            deadString = transform.GetComponentInParent<ParentReference>().reference;
            deadString.SetActive(true);
        }

    }
}
