using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {

    [HideInInspector] public bool jump;

    public float moveForce = 300f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform characterCollider;
    private bool grounded = false;
    private Rigidbody2D rigibody;

    void Awake()
    {
        rigibody = GetComponent<Rigidbody2D>();
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        grounded = Physics2D.Linecast(transform.position, characterCollider.position,1 << LayerMask.NameToLayer("ground"));

        Debug.Log("testeeeee: "+grounded);

        if(Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
        }
	}

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");

        if (h * rigibody.velocity.x < maxSpeed)
        {
            rigibody.AddForce(Vector2.right * h * moveForce);
        }

        if(h * rigibody.velocity.x > maxSpeed)
        {
            rigibody.velocity = new Vector2(Mathf.Sign(rigibody.velocity.x) * maxSpeed, rigibody.velocity.y);
        }

        if (jump)
        {
            rigibody.AddForce(new Vector2(0f, jumpForce));
            jump = false;
        }
    }
}
